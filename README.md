# Node.js Express Project

This is a simple Node.js project built with Express.js for handling form submissions.

## Installation

To run this project locally, follow these steps:

1. Clone the repository to your local machine:

   ```bash
   git clone <repository-url>
   
2. Navigate to the project directory:

   bash
   cd node-express-project

3. Install the dependencies using npm:

   bash
   npm install

4. Usage

   To start the application, run the following command:
   node app.js

   This will start the Node.js server, and you can access the application in your web browser at http://localhost:3000.

5. Features

   Handles form submissions using Express.js
   Uses body-parser middleware for parsing form data
   Simple HTML form for submitting data 

6. Dependencies

   This project relies on the following npm packages:

   Express.js: Fast, unopinionated, minimalist web framework for Node.js
   body-parser: Node.js body parsing middleware

7. Continuous Integration and Deployment (CI/CD) Pipeline

   This project includes a CI/CD pipeline configured with GitLab CI/CD. The pipeline performs the following steps:

   Build Stage: Installs dependencies using npm and caches node_modules directory.
   Deploy Stage: Deploys the application using Node.js. After successful build, the application is started, allowing it to be       
   accessed via the specified URL.










